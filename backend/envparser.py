# EnvParser
#
# Purpose: To extract environment variables from an input file


class EnvParser:
    def __init__(self, path):
        self.path = path
        self.var = dict()

    def get_variables(self):
        file = open(self.path, 'r')
        for line in file:
            attr = line.strip().split('=')      # remove newline
            if attr[0][0] == '#':
                continue                        # skip comments
            elif attr[0] == "DB_PORT":
                attr[1] = int(attr[1])          # convert port number to integer type
            self.var[attr[0]] = attr[1]         # add env variable to dictionary
        return self.var