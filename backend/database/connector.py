# Connector
# Purpose: To initialize a mysql connection using environment variables passed in
# Based on Connector.py in kyblit 2020summer main repo
# Michael Ruiz
import mysql.connector
from mysql.connector import Error
from mysql.connector import pooling


class ApiConnector:
    def __init__(self, envData: dict):
        self.env = envData
        self.conn = self.connect()

    def connect(self):
        db_host = self.env['DB_HOST']
        db_name = self.env['DB_NAME']
        db_port = self.env['DB_PORT']
        db_user = self.env['DB_USER']
        db_pass = self.env['DB_PASS']
        db_type = self.env['DB_TYPE']           # mariadb or mysql
        secret = self.env['SECRET_KEY']         # api secret

        try:
            pool = mysql.connector.pooling.MySQLConnectionPool(pool_name="apiPool",
                                                               pool_size=1,
                                                               pool_reset_session=True,
                                                               host=db_host,
                                                               port=db_port,
                                                               database=db_name,
                                                               user=db_user,
                                                               password=db_pass)#,
                                                               # auth_plugin=mysql_auth_pswd)
            return pool.get_connection()
        except Error as e:
            print(e)
            return

    def get_connection(self):
        return self.conn