## Getting Started
First you should rename the `sample.env` file to `.env` and customize the variables. <br>
Run application.py from command line or IDE to run flask server. <br>

## Webpage
Navigate to `localhost:5000/api?q=<desired word>` to view a list of related words. <br>
Webpage is not fully functional.

## API
Use the following command from command line to use API:
`curl localhost:5000/api?q=<desired word>`

