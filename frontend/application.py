from flask import Flask, render_template, url_for, request, session, redirect
from os import urandom
from backend.envparser import EnvParser
from backend.service.service import HomeService
from backend.database.connector import ApiConnector
import json


envParse = EnvParser('../.env')
env = envParse.get_variables()
api_conn = ApiConnector(env)
conn = api_conn.get_connection()
application = Flask(__name__)
application.secret_key = env['SECRET_KEY']


@application.route("/")
def home():
    page_text = HomeService().get_text()
    return render_template('index.html', active='home', page_text=page_text)


@application.route("/api", methods=['GET'])
def api():
    data = request
    if data.args['q']:
        q = data.args['q']
        cursor = conn.cursor()
        sql = f"SELECT name, id FROM ky_object a WHERE (a.id IN " \
              f"(SELECT result FROM ky_object_relation b WHERE b.lookup_id = " \
              f"(SELECT id FROM ky_object WHERE name = '{q}')));"
        cursor.execute(sql)
        records = cursor.fetchall()
        return json.dumps(records)
    print(data)
    return data.args['q']
    # return str(data['JSON'])


@application.route("/<page>")
def undefined(page):
    return render_template("undefined.html", page=page)


def run():
    application.run(debug=True)


if __name__ == '__main__':
    run()
